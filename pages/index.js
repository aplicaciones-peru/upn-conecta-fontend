import { useState } from 'react';
import Simple from '@/components/layouts/simple'
import css from './sass/login.module.sass'
import classnames from 'classnames'
import Login from '@/components/login'


import Container from 'react-bootstrap/Container'
import Carousel from 'react-bootstrap/Carousel'
import Image from 'react-bootstrap/Image'

var fase = 1

const fondo = (leyenda)=>(
	<Carousel className={css.fondoCarrusel} controls={false} fade={true} indicators={false}>
		<Carousel.Item>
			<figure>
				<picture>
					<source media="(max-width: 768px)" srcSet="/img/fondo1.jpg" />
					<source media="(min-width: 768px)" srcSet="/img/inicio_bg_1.jpg" />
					<Image src="/img/fondo1.jpg" fluid/>
				</picture>
				<If  condition={leyenda} >
					<figcaption>
						<p>Podrás ganar vales de <em>Plaza Vea</em>, suscripciones en <strong>Spotify</strong> y tarjetas de alimentos <em>Edenred</em></p>
						<small>* Válido en locales afiliados en todo el pais como KFC, Pizza Hut y muchos mas</small>
					</figcaption>
				</If>
			</figure>
		</Carousel.Item>
		<Carousel.Item>
			<figure>
				<picture>
					<source media="(max-width: 768px)" srcSet="/img/fondo2.jpg" />
					<source media="(min-width: 768px)" srcSet="/img/inicio_bg_2.jpg" />
					<Image src="/img/fondo2.jpg" fluid/>
				</picture>
				<If  condition={leyenda} >
					<figcaption>
						<p>Visita nuestras <span>Facultades</span></p>
						<p>Asiste a las charlas y participa <br/>en divertidos juegos</p>
					</figcaption>
				</If>
			</figure>
		</Carousel.Item>
	</Carousel>
)


const header = ()=>(
	<Image className={css.logo} src="/img/logotema.png" fluid/>
)

const Pagina = () => {

	const[ leyenda, cambiarLeyenda ] = useState(true)

	return <>
		<Simple className={css.pagina} class="bgAmarillo" fondo={fondo(leyenda)} header={header()}>
			<Container fluid className={classnames("pb-4")}>
				<Login leyenda={ ver => cambiarLeyenda(ver) }/>
			</Container>
		</Simple>
	</>
}
export default Pagina;
