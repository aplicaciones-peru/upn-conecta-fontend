import Link from 'next/link'

import { useState, useRef, useEffect  } from 'react';
import Simple from '@/components/layouts/simple'
import css from './sass/instrucciones.module.sass'
import classnames from 'classnames'
import Login from '@/components/login'

import { withRouter, useRouter } from 'next/router'


import Container from 'react-bootstrap/Container'
import Carousel from 'react-bootstrap/Carousel'
import Image from 'react-bootstrap/Image'


const Pagina = () => {

	const [index, setIndex] = useState(0);
	const router = useRouter()

	const handleSelect = (selectedIndex, e) => {
		setIndex(selectedIndex);
	};

	const siguienteClick = evt => {
		if ( index == 0) {
			setIndex(1);
		}
		else{
			router.push("/inicio")
		}
	}

	return (
	<Simple className={css.pagina}>
		<div className={css.contenedor}>
			<div className={css.contenido}>
				<Carousel
					activeIndex={index}
					onSelect={handleSelect}
					className={css.carrusel}
					controls={false}
					fade={false}
					indicators={true}
					interval={null}>
					<Carousel.Item>
						<div className={css.item}>
							<div className={css.texto}>
								<h1>Gracias por visitar UPN Conecta</h1>
								<p>Acumularás puntos navegando y jugando por nuestra web,<br/>
									con solo 500 puntos ingresarás automáticamente a un sorteo para poder ganar:</p>
								<h2>
									Vales de consumo para <em>Plaza Vea</em><br/>
									Suscripciones para <strong>Spotify</strong><br/>
									Tarjetas de alimentos <em>Edenred</em>
								</h2>
								<h3><strong>Fecha del Sorteo:</strong> 7 de Diciembre</h3>
								<small>Nuestros asesores se comunicarán con los ganadores.</small>
							</div>
						</div>
					</Carousel.Item>
					<Carousel.Item>
						<div className={css.item}>
							<div className={css.texto}>

								<p>Estas son las secciones que te dan puntos:</p>
								<ul>
									<li><span>Razones para estudiar en UPN</span><strong>100 puntos</strong></li>
									<li><span>Contacto con un asesor</span><strong>100 puntos</strong></li>
									<li><span>Cita vocacional</span><strong>100 puntos</strong></li>
									<li><span>Videojuego UPN Conecta</span><strong>100 puntos</strong></li>
									<li><span>Charlas</span><strong>100 puntos</strong></li>
									<li><span>Testimoniales</span><strong>100 puntos</strong></li>
								</ul>
								<p>Usa nuestros botones para poder navegar por UPN Conecta:</p>
								<div className={css.uso}>
									<div className={css.parrafo}>HAZ CLIC AQUÍ PARA JUGAR, CONTACTAR A UN ASESOR, VER EL VIDEO UPN Y PEDIR UNA CITA VOCACIONAL.</div>
									<div className={css.icono}>
										<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
											<rect y="14" width="32" height="3" rx="1.5" fill="black"/>
											<rect x="14" y="32" width="32" height="3" rx="1.5" transform="rotate(-90 14 32)" fill="black"/>
										</svg>

									</div>

									<div className={css.parrafo}>ACÁ PODRÁS VER TUS PUNTOS Y NUESTRAS FACULTADES</div>
									<div className={css.icono}>
										<svg width="32" height="19" viewBox="0 0 32 19" fill="none" xmlns="http://www.w3.org/2000/svg">
											<rect width="32" height="3" rx="1.5" fill="black"/>
											<rect y="8" width="32" height="3" rx="1.5" fill="black"/>
											<rect y="16" width="32" height="3" rx="1.5" fill="black"/>
										</svg>

									</div>

								</div>
							</div>
						</div>
					</Carousel.Item>
				</Carousel>

				<div className={css.botones}>
					<button type="button" onClick={siguienteClick} className={css.siguiente}>
						<span>Siguiente</span></button>
				</div>
			</div>
		</div>
	</Simple>
)
}

export default Pagina;
