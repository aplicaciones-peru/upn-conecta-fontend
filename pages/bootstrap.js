import React from 'react';

import Container from 'react-bootstrap/Container';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';

const Pagina = () => (
	<Container className="p-3">
		<Jumbotron>
			<h1 className="header">React-Bootstrap</h1>
			<p>https://react-bootstrap.github.io/layout/grid/</p>
		</Jumbotron>
	</Container>
)
export default Pagina;
