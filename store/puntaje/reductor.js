import { tipos } from './accion';

const inicial = {
	total: 0,
	ultimo: 0
}


const reducer = (state = inicial, action) => {
	switch (action.type) {
		case tipos.ACTUALIZAR:
			return {
				total: action.payload.total,
				ultimo: action.payload.ultimo
			};

		case tipos.ESTABLCER:
			console.debug("establece puntaje:", action.payload)
			return {
				...state,
				total: action.payload.total,
				ultimo: action.payload.ultimo
			};

		default: return state;
	}
};

export default reducer;
