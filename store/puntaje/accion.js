import { actualizar, consultarJuego } from "api/puntaje"
import { anunciarLogro } from "store/sesion/accion"

export const tipos = {
	ACTUALIZAR: 'PUNTAJE:ACTUALIZAR',
	ESTABLCER: 'PUNTAJE:ESTABLCER',
	CONSULTAR: 'PUNTAJE:CONSULTAR',
}

export const actualizarPuntaje =
	(evento, puntos) =>
		(dispatch, getState) => {

			// const estado = getState()
			// // const id = estado.usuario.id
			// console.log(estado)
			// console.debug("actualizarPuntaje", puntos)

			actualizar(evento, puntos)
				.then(res => {

					dispatch({
						type: tipos.ACTUALIZAR,
						payload: {
							total: res.puntos,
							ultimo: puntos
						}
					})
					if(res.estado == "OK"){
						dispatch(anunciarLogro(true))
					}

				})
				.catch(res => {console.log(res);})
		}

const establecerPuntaje =
	(puntos, ultimos = 0, logro = false) =>
		(dispatch, getState) => {


			const accion = dispatch({
				type: tipos.ESTABLCER,
				payload: {
					total: puntos,
					ultimo: ultimos,
				}
			})

			//dispatch(anunciarLogro(logro))
			return accion

		}

export {establecerPuntaje}

export const consultarPuntajeJuego =
	() =>
		(dispatch, getState) => {

			console.debug("consultarPuntajeJuego")

			const estado = getState()
			const ant = estado.puntaje.ultimo;
			// // const id = estado.usuario.id
			// console.log(estado)

			consultarJuego()
				.then(res => {

					console.debug("consultarPuntajeJuego", res)
					// establecerPuntaje(res.puntos, res.juego, ant == 0 && res.juego > 0)
					dispatch({
						type: tipos.ESTABLCER,
						payload: {
							total: res.total,
							ultimo: res.juego,
						}
					})

					dispatch(anunciarLogro(ant == 0 && res.juego > 0))


				})
				.catch(res => {console.log(res);})
		}
