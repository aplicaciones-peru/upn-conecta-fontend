// import reductores from './reductores/';
// import thunk from 'redux-thunk';
// import { createStore, applyMiddleware, compose } from 'redux';

// const iniciarAlmacen = (initialState, options) => {
// 	let componer = compose

// 	if (!options.isServer) {
// 		componer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
// 	}

// 	const store = createStore(rootReducer, initialState, componer(
// 		applyMiddleware(thunk) //Applying redux-thunk middleware
// 	));

// 	return store;
// };


// export default iniciarAlmacen;

import { createStore, applyMiddleware, combineReducers } from 'redux'
import { HYDRATE, createWrapper } from 'next-redux-wrapper'
import thunkMiddleware from 'redux-thunk'
import puntaje from './puntaje/reductor'
import usuario from './usuario/reductor'
import sesion from './sesion/reductor'

const bindMiddleware = (middleware) => {
	if (process.env.NODE_ENV !== 'production') {
		const { composeWithDevTools } = require('redux-devtools-extension')
		return composeWithDevTools(applyMiddleware(...middleware))
	}
	return applyMiddleware(...middleware)
}

const combinedReducer = combineReducers({
	puntaje,
	usuario,
	sesion
})

const reducer = (state, action) => {
	if (action.type === HYDRATE) {
		const nextState = {
			...state, // use previous state
			...action.payload, // apply delta from hydration
		}
		//if (state.count.count) nextState.count.count = state.count.count // preserve count value on client side navigation
		return nextState
	} else {
		return combinedReducer(state, action)
	}
}

export const store = createStore(reducer, bindMiddleware([thunkMiddleware]))

const initStore = () => store

export const wrapper = createWrapper(initStore)
