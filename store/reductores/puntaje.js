import * as valores from 'store/valores';

const inicial = {
	puntos: 0
}


const reducer = (state = inicial, action) => {
	switch (action.type) {
		case valores.ACTUALIZAR_PUNTAJE:
			return {
				puntos: state.puntos + 1
			};

		default: return state;
	}
};

export default reducer;
