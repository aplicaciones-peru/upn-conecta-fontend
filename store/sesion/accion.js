// import { actualizar } from "api/puntaje"

export const tipos = {
	LOGRO: 'SESION:LOGRO',
	LOGRO2: 'SESION:LOGRO2',
	FACULTAD: 'SESION:FACULTAD',
	AVANZA: 'SESION:AVANZA',
	NIVEL: 'SESION:NIVEL',
	INICIAR: 'SESION:INICIAR',
	TERMINAR: 'SESION:TERMINAR',
}

export const anunciarLogro =
	(logrado) =>
		(dispatch, getState) => {
			const estado = getState()
			if ( estado.puntaje.total >= 500 && !estado.sesion.victoria ) {
				dispatch({
					type: tipos.LOGRO2,
					payload: logrado
				})
			}
			else{
				dispatch({
					type: tipos.LOGRO,
					payload: logrado
				})

			}
		}
export const anunciarLogro2 =
	(logrado) =>
		(dispatch, getState) => {

			dispatch({
				type: tipos.LOGRO2,
				payload: logrado
			})
		}

export const modificarFacultad =
	(facultad) =>
		(dispatch, getState) => {

			dispatch({
				type: tipos.FACULTAD,
				payload: facultad
			})
		}


export const avanzarNivel =
() =>
	(dispatch, getState) => {

		dispatch({
			type: tipos.AVANZA
		})
	}

export const actualizarNivel =
	(valor) =>
		(dispatch, getState) => {

			dispatch({
				type: tipos.NIVEL,
				payload: valor
			})
		}

export const iniciarSesion =
	(facultad) =>
		(dispatch, getState) => {

			dispatch({
				type: tipos.INICIAR
			})
		}

export const terminarSesion =
	(facultad) =>
		(dispatch, getState) => {

			dispatch({
				type: tipos.TERMINAR
			})
		}
