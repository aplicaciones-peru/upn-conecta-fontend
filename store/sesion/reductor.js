import { tipos } from './accion';

const inicial = {
	logro: false,
	logro2: false,
	victoria: false,
	iniciada: false,
	facultad: "",
	nivel: 1
}


const reducer = (state = inicial, action) => {
	switch (action.type) {
		case tipos.LOGRO:
			return {
				...state,
				logro: action.payload
			};
		case tipos.LOGRO2:
			return {
				...state,
				logro2: action.payload,
				victoria: true
			};
		case tipos.FACULTAD:
			return {
				...state,
				facultad: action.payload
			};
		case tipos.AVANZA:
			return {
				...state,
				nivel: state.nivel + 1
			};
		case tipos.NIVEL:
			return {
				...state,
				nivel: action.payload
			};

		case tipos.INICIAR:
			return {
				...state,
				iniciada: true
			};
		case tipos.TERMINAR:
			return {
				...state,
				iniciada: true
			};

		default: return state;
	}
};

export default reducer;
