
import * as valores from './valores';

export const validarUsuario = () => ({ type: valores.ACTUALIZAR_USUARIO });
export const validarIdentidad = () => (dispatch) => {

	dispatch(validarUsuario);
};
