
import { establecerPuntaje } from "store/puntaje/accion"

export const tipos = {
	VALIDAR: 'USUARIO:VALIDAR',
	ACTUALIZAR: 'USUARIO:ACTUALIZAR',
}

export const actualizarDatos =
	(datos) =>
		(dispatch) => {

			// actualizar()
			dispatch(establecerPuntaje(datos.puntos))

			return dispatch({
				type: tipos.ACTUALIZAR,
				payload: {
					id: datos.id,
					nombres: datos.nombres
				}
			})
		}
