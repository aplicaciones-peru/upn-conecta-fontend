import { useEffect  } from 'react';
import styles from './estilo.module.sass'
import classnames from 'classnames';

import { useSelector, shallowEqual, useDispatch } from 'react-redux'
import { anunciarLogro } from 'store/sesion/accion'



const component = ({className, cerrar}) => {

	const dispatch = useDispatch()

	useEffect(() => {
		dispatch(anunciarLogro(false))
	},[]);

	const {puntos} = useSelector(
		(state) =>({
			puntos: state.puntaje.ultimo
		}),
		shallowEqual
	)

	return <>

		<div className={styles.cuadro}>
			<div className={styles.contenedor}>
				<button className={styles.cerrar} onClick={cerrar}>
				<svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
					<circle cx="30" cy="30" r="30" fill="black"/>
					<rect x="18" y="39.8996" width="32" height="3" rx="1.5" transform="rotate(-45 18 39.8996)" fill="#FDBA30"/>
					<rect x="40.6274" y="42.728" width="32" height="3" rx="1.5" transform="rotate(-135 40.6274 42.728)" fill="#FDBA30"/>
				</svg>
				</button>
				<div className={styles.contenido}>
						<h2>¡GANASTE!</h2>
						<div className={styles.texto}>
							{puntos}
						</div>
						<h3>PUNTOS</h3>
				</div>
			</div>
		</div>
	</>

}

export default component
