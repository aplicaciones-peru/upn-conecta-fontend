import React, { Component } from 'react';
import styles from './trivia.module.css';
import Button from '../button';

export default class Trivia extends Component {
  constructor(props) {
        super(props);
  }
  render() {
    return (<div className={styles.container}>
              <div className={styles.closeButton}></div>
              <div>
                <h1>NIVEL 1</h1>
                <h2>Selecciona las 2 opciones correctas</h2>
              </div>
              <div className={styles.preguntas_container}>
                <div className={styles.pregunta_fila}>
                  <div className={styles.pregunta}>Arnold Schwarzenegger es profesor de Educación Física</div>
                  <div className={styles.pregunta}>Facultades en todos los planetas.</div>
                </div>
                <div className={styles.pregunta_fila}>
                  <div className={styles.pregunta}>Prestigio y alta calidad certificada.</div>
                </div>
                <div className={styles.pregunta_fila}>
                  <div className={styles.pregunta}>Te pagan por estudiar</div>
                  <div className={styles.pregunta}>Innovador modelo educativo.</div>
                </div>
              </div>

            </div>);
  }
}
