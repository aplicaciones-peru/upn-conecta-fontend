import { useState, useEffect } from 'react'
import css from './estilo.module.sass'
import classnames from 'classnames';
import YouTube from 'react-youtube'
import { actualizarPuntaje } from 'store/puntaje/accion'
import { useDispatch } from 'react-redux'

import { registrarEvento } from 'lib/ga'

const component = ({className, cerrar, videos, facultad, titulo, charla}) => {
	// https://img.youtube.com/vi/<insert-youtube-video-id-here>/default.jpg
		const dispatch = useDispatch()
	const [actual, cambiarActual] = useState(-1)
	const opts = {
			height: '100%',
			width: '100%',
			playerVars: { autoplay: 0, rel:0 },
	    };
	useEffect(() => {
		if (videos.length) {
			cambiarActual(0)
		};
		return () => {
			cambiarActual(-1)
		};
	}, []);

	const videoActual = () => (
		actual >= 0
		? `${videos[actual].id}`
		: ''
	)
	const videoCharla = () => (`https://www.youtube.com/embed/${charla}`)

	const botones = videos.map((video, i) => (
		<button type="button" className={classnames({[css.activo]:(actual === i)})} onClick={() => cambiarActual(i)}>
			<img src={`https://img.youtube.com/vi/${video.id}/mqdefault.jpg`} alt=""/>
			<span>{video.titulo}</span>
		</button>
	))
/*
{ titulo == "Charlas" &&
	<div className={css.texto_charlas}>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </div>
}

	<iframe width="100%" height="100%" src={videoCharla()} frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
*/
	return <>
		<div className={css.cuadro}>
			<div className={css.contenedor}>

				<div className={css.contenido}>
				<button className={css.cerrar} onClick={cerrar}>
					<svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
					<circle cx="30" cy="30" r="30" fill="black"/>
					<rect x="18" y="39.8996" width="32" height="3" rx="1.5" transform="rotate(-45 18 39.8996)" fill="#FDBA30"/>
					<rect x="40.6274" y="42.728" width="32" height="3" rx="1.5" transform="rotate(-135 40.6274 42.728)" fill="#FDBA30"/>
					</svg>
				</button>
					<div className={css.video}>
					{ titulo == "Testimoniales" &&
							<YouTube
							videoId={videoActual()}
							containerClassName={css.visor}
							className={css.video}
							opts={opts}
							onStateChange={(e)=>{
							}}
							onPlay={()=>{
								registrarEvento("Testimoniales " + facultad, actual + " play")

							}}
							onEnd={()=>{
									dispatch(actualizarPuntaje(actual+"-"+facultad+"-testimoniales", 50))
									registrarEvento("Testimoniales " + facultad, actual + "  end")
							}}
								onReady={()=>{
								console.log("Video onReady");
							}} />
					}
					{ titulo == "Charlas" &&
						<YouTube
						videoId={charla}
						containerClassName={css.visor}
						className={css.video}
						opts={opts}
						onStateChange={(e)=>{
						}}
						onPlay={()=>{
							registrarEvento("Charlas " + facultad, "play")

						}}
						onEnd={()=>{
								dispatch(actualizarPuntaje(facultad+"-charlas", 100))
								registrarEvento("Charlas " + facultad, "end")
						}}
							onReady={()=>{
							console.log("Video onReady");
						}} />
					}
					</div>
					<div className={css.disclaimer_movil}>No te pierdas todo el contenido del video y gana más puntos!</div>
					<div className={css.titulo}>
						<h3>Facultad de {facultad}</h3>
						<h1>{titulo}</h1>
						<div className={css.disclaimer_desktop}>No te pierdas todo el contenido del video y gana más puntos!</div>
					</div>

					{ titulo == "Testimoniales" &&
						<div className={css.videos}>{botones}</div>
					}

				</div>
			</div>
		</div>
	</>

}

export default component
