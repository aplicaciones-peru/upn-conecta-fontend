import React, { Component } from 'react';
import styles from './popupvideo.module.css';
import Button from '../button';
/*

*/
export default class PopupVideo extends Component {
  constructor(props) {
        super(props);
  }

  render() {
    return (<div className={styles.container} style={{backgroundColor:(this.props.tipo=="negro")?"black":"#FDBA30"}}>
              <div className={styles.closeButton}></div>

              {(this.props.tipo=="testimonial" || this.props.tipo=="charlas") &&
                <div className={styles.video}></div>
              }

              <div className={styles.popDescription} style={{flexDirection:(this.props.tipo=="testimonial" || this.props.tipo=="charlas")?"row":"column"}}>
                <div className={styles.l1}>
                  Facultad de xxxxxxxxxxxxxx<br/>
                  {this.props.tipo=="testimonial" && <h1>TESTIMONIALES</h1>}
                  {this.props.tipo=="charlas" && <h1>CHARLAS</h1>}
                  {this.props.titulo!="" && <h1>{this.props.titulo}</h1>}
                </div>

                {this.props.tipo=="testimonial" &&
                <div className={styles.l2}>
                  <div className={styles.thumbnail}>
                    <div className={styles.image}></div>
                    <div style={{marginTop:3}}>ALUMNO</div>
                  </div>
                  <div className={styles.thumbnail}>
                    <div className={styles.image}></div>
                    <div style={{marginTop:3}}>EX ALUMNO</div>
                  </div>
                </div>
                }
                {this.props.tipo=="charlas" &&
                <div className={styles.l3}>
                  <div>
                      Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                  </div>
                </div>
                }
                {this.props.tipo=="texto" &&
                <div className={styles.l3}>
                  <hr/>
                  <ul>
                      <li>Lorem ipsum dolor sit amet.</li>
                      <li>Lorem ipsum dolor sit amet.</li>
                      <li>Lorem ipsum dolor sit amet.</li>
                      <li>Lorem ipsum dolor sit amet.</li>
                      <li>Lorem ipsum dolor sit amet.</li>
                      <li>Lorem ipsum dolor sit amet.</li>
                      <li>Lorem ipsum dolor sit amet.</li>
                  </ul>
                </div>
                }


              </div>
            </div>);
  }
}
