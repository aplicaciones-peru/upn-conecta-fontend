import styles from './loginbox.module.css'

export default function Loginbox({ children }) {
  return (<div>
  		  <div className={styles.container}>
  			<div className={styles.barra}></div>
  			<h1>INGRESA TUS DATOS</h1>
  			para entrar a UPN CONECTA.
  			<div className={styles.frm_control}>
  				Celular de tu apoderado
  				<input type="text"/>
  			</div>
  			<div className={styles.frm_control}>
  				Tu correo
  				<input type="text"/>
  			</div>
  			<div className={styles.error}></div>
  			<button className={styles.boton}>INGRESA</button>
  		 </div>
  		 <div className={styles.container}>
  			<div className={styles.barra}></div>
  			<h1>ACTUALIZA TUS DATOS</h1>
  			para entrar a UPN CONECTA.
  			<div className={styles.frm_control}>
  				Tu nombre
  				<input type="text"/>
  			</div>
  			<div className={styles.frm_control}>
  				Tus apellidos
  				<input type="text"/>
  			</div>
  			<div className={styles.frm_control}>
  				Celular de tu apoderado
  				<input type="text"/>
  			</div>
  			<div className={styles.frm_control}>
  				Tu correo
  				<input type="text"/>
  			</div>
  			<div className={styles.error}></div>
  			<button className={styles.boton} style={{color:"#EFA50D"}}>¡CONÉCTATE!</button>
  		 </div>
  		 </div>)
}
