import { useState } from 'react';
import styles from './estilo.module.css'
import classnames from 'classnames'

import { useForm } from 'react-hook-form';
import { registrar } from 'api/prospecto';
import isEmail from 'validator/lib/isEmail'


import Spinner from 'react-bootstrap/Spinner'

export default function Loginbox({ onContinuar, datos }) {
	const { register, handleSubmit, errors } = useForm({
		defaultValues: datos
	});

	const [cargando, actualizarCargando] = useState(false)

	const continuar = info => {
		// console.log(info)
		// const requestOptions = {
		// 	method: 'PUT',
		// 	headers: { 'Content-Type': 'application/json' },
		// 	body: info //JSON.stringify(info)
		// };

		// fetch(`${urlBase}/prospecto/${datos.id}`, requestOptions)
		// 	.then((response) => {
		// 		return response.json()
		// 	})
		// 	.then((json) => {
		// 		onContinuar()
		// 	})

		// const headers = { 'Content-Type': 'application/json' }

		// axios.post(`${urlBase}/prospecto/${datos.id}`, info, { headers })
		// 	.then(response => {
		// 		console.log(response)
		// 		onContinuar()
		// 	})

		actualizarCargando(true)
		registrar(info)
			.then(response => {
				console.log(response)
				console.log(onContinuar)
				actualizarCargando(false)
				onContinuar(response)
			})

	}

	return <>
	<div className={styles.container}>
		<div className={styles.barra}></div>
		<h1>INGRESA TUS DATOS</h1>
		para entrar a UPN CONECTA.

		<form onSubmit={handleSubmit(continuar)}>
			<div className={styles.frm_control}>
				Tu nombre
				<input
					name="nombres"
					className={classnames({[styles.error]:errors.nombres})}
					ref={register({
						required: true,
						maxLength: 48,})}
					type="text"
					maxLength="24"
				/>
			</div>
			<div className={styles.frm_control}>
				Tu apellido paterno
				<input
					name="apellidoPaterno"
					className={classnames({[styles.error]:errors.apellidoPaterno})}
					ref={register({
						required: true,
						maxLength: 24,})}
					type="text"
					maxLength="24"

				/>
			</div>
			<div className={styles.frm_control}>
				Tu apellido materno
				<input
					name="apellidoMaterno"
					className={classnames({[styles.error]:errors.apellidoMaterno})}
					ref={register({
						required: true,
						maxLength: 24,})}
					type="text"
				/>
			</div>
			<div className={styles.frm_control}>
				Tu celular
				<input
					name="celular"
					className={classnames({[styles.error]:errors.celular})}
					maxLength="9"
					ref={register({
						required: true,
						minLength: 9,
						maxLength: 9,
						pattern: /^[0-9]+$/i
					})}
					type="tel"
				/>
			</div>
			<div className={styles.frm_control}>
				Tu correo
				<input
					name="email"
					className={classnames({[styles.error]:errors.email})}
					ref={register({
						required: true,
						validate: (input) => isEmail(input)
					})}
					type="email"
				/>
			</div>
			<div className={styles.error}></div>
			<button className={styles.boton} type="submit">
				<span>¡CONÉCTATE!</span>
				<Spinner className={classnames(styles.spinner, {[styles.activo]: cargando})} animation="border" size="sm" />
			</button>
		</form>
	</div>
	</>
}
