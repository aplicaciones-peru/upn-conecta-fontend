import { useState, useEffect, useRef } from 'react'
import css from './estilo.module.sass'
import classnames from 'classnames';
import Button from '../button';


import { useSelector, shallowEqual } from 'react-redux'

//Supera las dificultades y contesta las preguntas clave para pasar de nivel. Por cada nivel superado acumula 50 puntos. Si pierdes puedes volver a intentarlo!
const component = ({className, cerrar, nombreFacultad}) => {

	const [showJuego,setShowJuego] = useState(false)
	//

	var f="0";
	var url_game = "";
	if(nombreFacultad=="derecho"){ f="0"; }
	if(nombreFacultad=="comunicaciones"){ f="1"; }
	if(nombreFacultad=="arquitecturadisegno"){ f="2"; }
	if(nombreFacultad=="ingenieria"){ f="3"; }
	if(nombreFacultad=="negocios"){ f="4"; }
	if(nombreFacultad=="salud"){ f="5"; }

	const {id} = useSelector(
		(state) =>({
			id: state.usuario.id,
		}),
		shallowEqual
	)
	url_game = `${process.env.NEXT_PUBLIC_URL_JUEGO}/?u=${id}&f=${f}`;
	return <>
		<div className={css.cuadro}>
			<div className={css.contenedor} style={{maxWidth:(!showJuego)?700:900}}>

				{ !showJuego &&
					<div className={css.contenido}>
					<button className={css.cerrar} onClick={() => cerrar(showJuego)}>
						<svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
						<circle cx="30" cy="30" r="30" fill="black"/>
						<rect x="18" y="39.8996" width="32" height="3" rx="1.5" transform="rotate(-45 18 39.8996)" fill="#FDBA30"/>
						<rect x="40.6274" y="42.728" width="32" height="3" rx="1.5" transform="rotate(-135 40.6274 42.728)" fill="#FDBA30"/>
						</svg>
					</button>
						<div className={css.razones}>
								<img src="/img/juego_titulo.svg"/>
						</div>

						<div className={css.texto} style={{color:"#000"}}>

							<div className={css.black_space}>
							La vida universitaria es toda una aventura. Acompaña a nuestro pequeño héroe a pasar obstáculos, recolectar diamantes y responder correctamente las preguntas de cada nivel para acumular la mayor cantidad de puntos. Son 3 niveles que te ayudarán a descubrir el mundo de UPN. Nuestro héroe avanza automáticamente, para saltar solo dale click al botón izquierdo del mouse o si estás en tu celular solo presiona la pantalla.
							</div>
							<Button style={{marginTop:10}} texto="COMENCEMOS" invert={true} over={false} onClick={()=>setShowJuego(true)}></Button>
						</div>
					</div>
				}
				{ showJuego &&
					<div className={classnames(css.contenido, css.jugando)}>
					<button className={css.cerrar} onClick={() => cerrar(showJuego)}>
						<svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
						<circle cx="30" cy="30" r="30" fill="black"/>
						<rect x="18" y="39.8996" width="32" height="3" rx="1.5" transform="rotate(-45 18 39.8996)" fill="#FDBA30"/>
						<rect x="40.6274" y="42.728" width="32" height="3" rx="1.5" transform="rotate(-135 40.6274 42.728)" fill="#FDBA30"/>
						</svg>
					</button>
						<div className={css.juego}>
							<iframe src={url_game}/>
						</div>
					</div>
				}


			</div>
		</div>
	</>

}

export default component
