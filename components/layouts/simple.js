// import { Component } from 'react'
import Pagina from "./base";
import classnames from 'classnames';
import css from "./simple.module.sass"


export default function Layout({ children, className, ...props }) {
	return <Pagina {...props} className={classnames(css.pagina, className)}>
		<If condition={ props.fondo }>
			<div className={css.fondo}>
				{props.fondo}
			</div>
		</If>
		<If condition={ props.header }>
			<header className={css.header}>
				{props.header}
			</header>
		</If>
		<main className={css.main}>
			{children}
		</main>
		<If condition={ props.footer }>
			<footer className={css.footer}>{props.footer}</footer>
		</If>

	</Pagina>
}
