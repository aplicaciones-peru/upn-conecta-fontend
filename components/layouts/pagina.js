import { useState, useEffect, useRef } from 'react'
import { useRouter } from 'next/router'
import Base from "./base";
import Header from "../header";
import Footer from "../footer";
import Menu from "../menu";
import Submenu from "../submenu";

import Cita from "components/cita"
import Video from "components/video"
// import Contacto from "components/contacto"
// import Juego from "components/juego"
import Trivia from "components/trivia"
import Ganaste from "components/ganaste"
import Lograste from "components/lograste"

import classnames from 'classnames';
import css from "./pagina.module.sass"


import { useSelector, shallowEqual, useDispatch } from 'react-redux'
import { actualizarPuntaje } from 'store/puntaje/accion'
import { telefonos } from 'lib/api'

import { event } from "next-ga/dist/analytics/prod";

import { registrarEvento } from 'lib/ga'


const usePrevious = (value) => {
	const ref = useRef();
	useEffect(() => {
		ref.current = value;
	});
	return ref.current;
}

export default function Layout({ children, className, footer, ...props }) {

	const [menu, cambiarMenu] = useState(false)
	const [subitem, cambiarSubitem] = useState("bienvenida")
	//const [subitem, cambiarSubitem] = useState("bienvenida")
	// && localStorage.getItem('bienvenida')!="bienvenida"
	const [movil, cambiarMovil] = useState(false)

	const prevSubitem = usePrevious(subitem)
	useEffect(() => {
		if( subitem == "contacto" ){
			const telefono = telefonos[Math.floor(Math.random() * telefonos.length)]
			const url_movil = "https://wa.me/+51"+telefono;
			cambiarSubitem("ninguno")
			dispatch(actualizarPuntaje(subitem, 100))
			if(movil){
				window.document.location.href = url_movil
			}else{
				window.open(url_movil, '_blank');
			}
		}

		// event("navegacion", "abrir " + subitem)

		if (subitem == "ninguno") {
			registrarEvento("navegacion", "cerrar " + prevSubitem)
			console.info("navegacion cerrar",  prevSubitem + " > " + subitem)
		}
		else{
			registrarEvento("navegacion", "abrir " + subitem)
			console.info("navegacion abrir",  prevSubitem + " > " + subitem)
		}

	}, [subitem])


	useEffect(() => {
		registrarEvento("menu", menu?"abierto":"cerrado")
	},[menu]);


	useEffect(() => {
		const matchMediaChange = e => { cambiarMovil(e.matches) }
		cambiarMovil(window.matchMedia("(max-width: 768px)").matches)
		window.matchMedia("(max-width: 768px)").addListener( matchMediaChange)
		return () => {window.matchMedia("(max-width: 768px)").removeListener(matchMediaChange)}
	},[]);

	const dispatch = useDispatch()

	const popupCerrar = (hito = false) => {
		console.debug("popupCerrar", hito)
		cambiarSubitem("ninguno")
		if( hito ){
			dispatch(actualizarPuntaje(subitem, 100))
		}
	}

	const triviaCerrar = (nivel, puntos) => {
		if (puntos > 0) {
			dispatch(actualizarPuntaje(`trivia-${nivel}`, puntos))
		}
		cambiarSubitem("ninguno")
	}


	const {logro, logro2, iniciada} = useSelector(
		(state) =>({
			logro: state.sesion.logro,
			logro2: state.sesion.logro2,
			iniciada: state.sesion.iniciada
		}),
		shallowEqual
	)

	useEffect(() => {
		if (logro2) {
			console.log("muestra puntos 2")
			cambiarSubitem("lograste")
		}
		if (logro) {
			console.log("muestra puntos")
			cambiarSubitem("ganaste")
		}
	})
	const router = useRouter()
	useEffect(() => {
		if (!iniciada) {
			// router.push("/")
		}
	})

	const storage = () => typeof window !== "undefined" ? window.localStorage : null
	const [bienvenida, actualizarBienvenida] = useState(false)
	useEffect(() => {
		// if (storage()["bienvenida"]) {

		// 	actualizarBienvenida(storage().getItem("bienvenida"))
		// 	console.debug("bienvenida", bienvenida)
		// }
		// else{
		// 	actualizarBienvenida()
		// }
		actualizarBienvenida(!storage()["bienvenida"])
	}, []);
	useEffect(() => {
		storage().setItem('bienvenida', bienvenida);
	}, [bienvenida]);

	return <Base {...props} className={classnames(css.pagina, className)}>

		<Header className={css.header}/>

		<div className={css.botonMenu}>
			<button type="button" onClick={() => cambiarMenu(true)}>
				<img src="/img/menu.svg" alt=""/>
			</button>
			<button className={css.botonWhatsapp} type="button" onClick={() => cambiarSubitem("contacto")}>
				<img src="/img/ico-wsp.svg" alt=""/>
			</button>
		</div>

		<main className={css.main}>
			{children}
		</main>

		<Submenu className={css.submenu} subitemClick={item =>  cambiarSubitem(item)} />
		<Footer className={css.footer} contenido={footer}>{footer}</Footer>
		<If condition={menu}>
			<Menu className={css.menu} menuCerrar={() => cambiarMenu(false) } />
		</If>

		<If condition={props.cuadros}>
			{props.cuadros}
		</If>

		<Choose>
			<When condition={(subitem == "bienvenida" && bienvenida )}>
				<Video modo="bienvenida" cerrar={()=>cambiarSubitem("ninguno") || actualizarBienvenida(false)} />
			</When>
			<When condition={(subitem == "cita")}>
				<Cita cerrar={()=>cambiarSubitem("ninguno")} />
			</When>
			<When condition={(subitem == "video")}>
				<Video modo="videotour" cerrar={()=>cambiarSubitem("ninguno")} />
			</When>
			<When condition={(subitem == "contacto")}>
				// whatsapp
			</When>
			<When condition={(subitem == "juego")}>
				<Trivia cerrar={triviaCerrar} />
			</When>
			<When condition={(subitem == "ganaste")}>
				<Ganaste cerrar={()=>cambiarSubitem("ninguno")} />
			</When>
			<When condition={(subitem == "lograste")}>
				<Lograste cerrar={()=>cambiarSubitem("ninguno")} />
			</When>
		</Choose>
	</Base>
}
