// import { Component } from 'react'
import Head from 'next/head'
import classnames from 'classnames'
import css from "./base.module.sass"

// import ReactGA from 'react-ga';


export default function Pagina({
	children,
	className,
	title = "UPN Conecta"
}) {
	// ReactGA.initialize('G-CE5RRYHKF7')
	// ReactGA.pageview(window.location.pathname + window.location.search)

	return <div className={classnames(css.pagina, className)}>
		<Head>
			<title>{title}</title>

			<script async src="https://www.googletagmanager.com/gtag/js?id=${process.env.NEXT_PUBLIC_GA_ID}"></script>
			<script dangerouslySetInnerHTML={{ __html: `
				window.dataLayer = window.dataLayer || [];
				function gtag(){dataLayer.push(arguments);}
				gtag('js', new Date());

				gtag('config', '${process.env.NEXT_PUBLIC_GA_ID}');
			` }} />

		</Head>
		{children}
	</div>

}
/*
<script dangerouslySetInnerHTML={{ __html: `
		let vh = window.innerHeight * 0.01 + 'px';
		document.documentElement.style.setProperty('--vh', vh);
	` }} />
	*/


// function Fondo() {
// 	return null
// }

// class Pagina extends Component {
// 	static Fondo = Fondo

// 	render() {
// 		const {children, title, className} = this.props
// 		const fondo = children.find(child => child.type == Fondo)

// 		return (
// 		<div className={classnames(css.pagina, className)}>
// 			<Head>
// 				{/* <title>{title}</title> */}
// 				<meta charSet="utf-8" />
// 				<meta name="viewport" content="initial-scale=1.0, width=device-width" />
// 			</Head>
// 			<If condition={ fondo }>
// 				<div className="fondo">{fondo.props.children}</div>
// 			</If>
// 			{children}
// 		</div>
// 		)
// 	}
//   }

// export default Pagina
