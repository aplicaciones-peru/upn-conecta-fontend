import { useState, useEffect } from 'react'
import css from './estilo.module.sass'
import classnames from 'classnames';
import YouTube from 'react-youtube'

import { registrarEvento } from 'lib/ga'

const opts = {
		height: '100%',
		width: '100%',
		playerVars: { autoplay: 0, rel:0 },
    };

const component = ({className, cerrar, modo}) => {

	return <>
		<div className={css.cuadro}>
			<div className={css.contenedor}>
				{modo=="videotour" && <div className={css.bienvenido}>Video UPN</div>}
				{modo=="bienvenida" && <>
					<div className={css.bienvenido}>Bienvenido a UPN Conecta</div>
					<div className={css.conoce_mas}>Conoce más sobre nosotros en este video</div>
				</>}

				{modo=="bienvenida" &&
					<YouTube
						videoId={"94iY-RF5HzE"}
						containerClassName={css.visor}
						className={css.video}
						opts={opts}
						onStateChange={(e)=>{
						}}
						onPlay={()=>{
								console.log("onPlay");
								registrarEvento("video bienvenida", "play")
						}}
						onEnd={()=>{
								console.log("onEnd");
								registrarEvento("video bienvenida", "end")
						}}
							onReady={()=>{
							console.log("Video onReady");
						}} />
				}
				{modo=="videotour" &&
					<YouTube
						videoId={"liRGQHyHCKc"}
						containerClassName={css.visor}
						className={css.video}
						opts={opts}
						onStateChange={(e)=>{
						}}
						onPlay={()=>{
								console.log("onPlay");
								registrarEvento("videotour", "play")

						}}
						onEnd={()=>{
								console.log("onEnd");
								registrarEvento("videotour", "end")
						}}
							onReady={()=>{
							console.log("Video onReady");
						}} />
				}

				<button className={css.cerrar} onClick={cerrar}>
					<svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
					<circle cx="30" cy="30" r="30" fill="black"/>
					<rect x="18" y="39.8996" width="32" height="3" rx="1.5" transform="rotate(-45 18 39.8996)" fill="#FDBA30"/>
					<rect x="40.6274" y="42.728" width="32" height="3" rx="1.5" transform="rotate(-135 40.6274 42.728)" fill="#FDBA30"/>
					</svg>
				</button>



			</div>
		</div>
	</>
}

export default component
