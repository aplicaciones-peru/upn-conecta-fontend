import styles from './estilo.module.sass'
import classnames from 'classnames';

const component = ({className, cerrar}) => {
	return <>
		<div className={styles.contenedor}>
			<button className={styles.cerrar} onClick={cerrar}>
				<img src="/img/cerrar.svg" alt=""/>
			</button>
			<div className={styles.contenido}>

					<div className={styles.texto}>
							<div className={styles.topText}>
							Para acumular puntos y ganar premios<br/> explora las siguientes secciones:<br/>
							</div>
							<ul>
							<li>
								<div className={styles.label}><div className={styles.circulo}></div><div className={styles.desc}>Razones para estudiar en UPN</div></div>
								<div className={styles.puntaje}>100 Puntos</div>
							</li>
							<li><div className={styles.label}><div className={styles.circulo}></div> Contacto con un asesor</div>  <div className={styles.puntaje}>100 Puntos</div></li>
							<li><div className={styles.label}><div className={styles.circulo}></div> Cita vocacional</div> <div className={styles.puntaje}>100 Puntos</div></li>
							<li><div className={styles.label}><div className={styles.circulo}></div> Videojuego UPN Conecta</div> <div className={styles.puntaje}>100 Puntos</div></li>
							<li><div className={styles.label}><div className={styles.circulo}></div> Charlas</div> <div className={styles.puntaje}>100 Puntos</div></li>
							<li><div className={styles.label}><div className={styles.circulo}></div> Testimoniales</div> <div className={styles.puntaje}>100 Puntos</div></li>
							</ul>

							<div className={styles.texto_acumula}>
							Participa y con solo acumular <b style={{color:"#FDBA30"}}>500 puntos</b> automáticamente entrarás al sorteo de:
							</div>

							<div className={styles.texto_premios}>
							Vales de consumo para <span style={{color:"red"}}>Plaza Vea</span><br/>
							Suscripciones para <span style={{color:"#0F901C"}}>Spotify</span><br/>
							Tarjetas de alimentos <span style={{color:"red"}}>Edenred</span>
							</div>

							<hr/>

							<div className={styles.fecha_sorteo}>
								<span style={{color:"#FDBA30"}}>Fecha del Sorteo:</span> 7 de Diciembre
							</div>
							<div className={styles.txt}>
								Nuestros asesores se comunicarán con los ganadores.
							</div>

						</div>

			</div>

		</div>
	</>

}

export default component
