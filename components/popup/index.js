import React, { Component } from 'react';
import styles from './popup.module.css';
import Button from '../button';

export default class Popup extends Component {
  constructor(props) {
        super(props);
  }

  render() {
    return (<div className={styles.container} style={{backgroundColor:(this.props.tipo=="negro")?"black":"#FDBA30"}}>
              <div className={styles.closeButton}></div>
              <div className={styles.razones}>
                {this.props.tipo=="negro" &&
                  <img src="/img/razones.svg"/>
                }
                {this.props.tipo=="juego" &&
                  <img src="/img/juego_titulo.svg"/>
                }
              </div>
              <div className={styles.separador} style={{backgroundColor:(this.props.tipo=="negro")?"#FFF":"#000"}}></div>
              <div className={styles.texto} style={{color:(this.props.tipo=="negro")?"#FFF":"#000"}}>

              {this.props.tipo=="negro" &&
              <div>En cada nivel encontrarás 2 razones reales para estudiar en UPN. Y en último nivel, el más difícil, habrá solo 1 razón. En total son 7 Razones. Identifícalas correctamente y acumula mayor puntaje.</div>
              }
              {this.props.tipo=="juego" &&
              <div>Supera las dificultades y contesta las preguntas clave para pasar de nivel. Por cada nivel superado acumula 50 puntos. Si pierdes puedes volver a intentarlo!</div>
              }
              <Button style={{marginTop:20}} texto="COMENCEMOS" invert={(this.props.tipo=="negro")?false:true}></Button>
              </div>
            </div>);
  }
}
