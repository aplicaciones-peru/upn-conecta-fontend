import { useState, useEffect } from 'react'
import styles from './estilo.module.sass'
import classnames from 'classnames';
import React, { Component } from 'react';
import Button from '../button';

import { useSelector, shallowEqual, useDispatch } from 'react-redux'
import { avanzarNivel } from 'store/sesion/accion'

const component = ({className, cerrar, modo}) => {

	const [puntos, actualizarPuntos] = useState(0);

	const {nivel} = useSelector(
		(state) =>({
			nivel: state.sesion.nivel,
		}),
		shallowEqual
	)

	const dispatch = useDispatch()
	const avanzaNivel = () => dispatch(avanzarNivel())

	return <>
		<div className={styles.cuadro}>
			<Trivia
				cerrar={() => cerrar(nivel, puntos)}
				nivel={nivel}
				avanzaNivel={avanzaNivel}
				actualizar={valor => actualizarPuntos(valor)} />
		</div>
	</>

}
class Item extends Component {
		constructor(props) {
			super(props);
			this.state = {correct:this.props.opcion.flag,selected:false}
		}
		render(){
			var bg = "#FDBA30";
			var texto = "black";
			if(this.state.selected){
				if(this.state.correct){
					bg = "#234898";
					texto = "white";
				}else{
					bg = "black";
					texto = "white";
				}
			}
			return <div  style={{backgroundColor:bg,color:texto}} className={styles.pregunta} onClick={()=>{
				if(!this.props.disabled)this.setState({selected:true});
				this.props.onSelected();
				if(this.state.correct){
					this.props.onOk();
				}

			}} >{this.props.opcion.texto}</div>
		}
}
class Trivia extends Component {
	constructor(props) {
		super(props);
		var data = new Array();
		var item = {};

		item.opcion1 = {texto : "Nuestros convenios internacionales son solo con universidades de Tazmania.",flag : false};
		item.opcion2 = {texto : "Nuestro prestigio y alta calidad están comprobados con certificaciones internacionales.",flag : true};
		item.opcion3 = {texto : "Hemos firmado alianzas con universidades de los 12 continentes.",flag : false};
		item.opcion4 = {texto : "Tenemos 320 carreras por facultad.",flag : false};
		item.opcion5 = {texto : "Tenemos un modelo educativo innovador y muy moderno.",flag : true};
		data.push(item);

		var item = {};
		item.opcion1 = {texto : "Enseñan solo profesores traídos de Grecia.",flag : false};
		item.opcion2 = {texto : "Contamos con una planta nuclear.",flag : false};
		item.opcion3 = {texto : "Nuestra infraestructura es de las más modernas del Perú.",flag : true};
		item.opcion4 = {texto : "Tenemos campus flotantes en todas las lagunas del Perú.",flag : false};
		item.opcion5 = {texto : "Nuestra educación virtual se da a través del mejor sistema multiplataforma.",flag : true};
		data.push(item);

		var item = {};
		item.opcion1 = {texto : "Nuestros egresados consiguen trabajo rápidamente.",flag : true};
		item.opcion2 = {texto : "Estamos certificados por la NASA.",flag : false};
		item.opcion3 = {texto : "Todas nuestras aulas cuentan con piscinas olímpicas. ",flag : false};
		item.opcion4 = {texto : "Ofrecemos experiencias de nivel internacional.",flag : true};
		item.opcion5 = {texto : "Todos nuestros profesores son expertos bailarines de marinera norteña.",flag : false};
		data.push(item);

		var item = {};
		item.opcion1 = {texto : "Nuestras aulas son de 700 estudiantes.",flag : false};
		item.opcion2 = {texto : "Podrás hacer tus prácticas profesionales en nuestra sede de Siberia.",flag : false};
		item.opcion3 = {texto : "Le brindamos el mejor soporte y acompañamiento al estudiante.",flag : true};
		item.opcion4 = {texto : "UPN tiene convenios con las mejores escuelas de surf.",flag : false};
		item.opcion5 = {texto : "El edificio de nuestra Facultad de Arte fue diseñado por Antonio Gaudí.",flag : false};
		data.push(item);

		//this.props.nivel
		var nivel = this.props.nivel;

		this.state = {layer:1,data:data,nivel:nivel,current_nivel:nivel,opciones:0,correctas:0, acumulado: 0};
		this.onPressNivel1 = this.onPressNivel1.bind(this);
		this.onPressNivel2 = this.onPressNivel2.bind(this);
		this.onPressNivel3 = this.onPressNivel3.bind(this);
		this.onPressNivel4 = this.onPressNivel4.bind(this);

		console.log("componentDidMount");
		console.log("current_nivel = "+this.state.current_nivel);
	}
	componentDidMount(){

	}
	onPressNivel1(){

		this.setState({layer:3,nivel:1});
	}
	onPressNivel2(){
		if(this.state.current_nivel)
		this.setState({layer:3,nivel:2});
	}
	onPressNivel3(){
		this.setState({layer:3,nivel:3});
	}
	onPressNivel4(){
		this.setState({layer:3,nivel:4});
	}
	checkCorrectas(){
		console.log("checkCorrectas");
		if(this.state.nivel==4){
			if(this.state.correctas==1){
				console.log("OK");
				setTimeout(()=>{
					this.setState({layer:2,opciones:0,correctas:0});
					console.log("!! "+this.state.current_nivel)
					console.log("-- "+this.state.nivel)
					//if(this.state.current_nivel>=this.state.nivel){

					//}else{
						var acumulado = this.state.acumulado+25;
						if(acumulado>100)acumulado=100;
						console.log("acumulado*** "+acumulado);
						this.setState({acumulado:acumulado});
						this.props.actualizar(this.state.acumulado)
					//}

				},2000);
			}else if(this.state.opciones==2){
				setTimeout(()=>{
					this.setState({layer:2,opciones:0,correctas:0});
				},1000);
			}else{

			}
		}else{
			console.log("this.state.opciones "+this.state.opciones);
			if(this.state.correctas==2){
				setTimeout(()=>{
					var next_level = 1;
					if(this.state.current_nivel>this.state.nivel){
						console.log("*1 -- ");
						this.setState({layer:2,opciones:0,correctas:0});
					}else{
						console.log("*2 -- ");
						this.setState({layer:2,opciones:0,correctas:0,current_nivel:this.state.current_nivel+1});
						this.props.avanzaNivel();
						var acumulado = this.state.acumulado+25;
						if(acumulado>100)acumulado=100;
						console.log("acumulado*** "+acumulado);
						this.setState({acumulado:acumulado});
						this.props.actualizar(this.state.acumulado)
					}

				},1000);
			}else if(this.state.opciones==2){
				setTimeout(()=>{
					this.setState({layer:2,opciones:0,correctas:0});
				},1000);
			}else{

			}
		}
	}

	render() {
		console.log("current_nivel--------- = "+this.state.current_nivel);
		var layer = (<div></div>);
		if(this.state.layer == 1){
			layer = (<div className={styles.contenedor}>
				<button className={styles.cerrar} onClick={this.props.cerrar}>
				<svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
				<circle cx="30" cy="30" r="30" fill="black"/>
				<rect x="18" y="39.8996" width="32" height="3" rx="1.5" transform="rotate(-45 18 39.8996)" fill="#FDBA30"/>
				<rect x="40.6274" y="42.728" width="32" height="3" rx="1.5" transform="rotate(-135 40.6274 42.728)" fill="#FDBA30"/>
				</svg>
				</button>
				<div className={styles.contenido}>
				<div className={styles.razones}>
					<img src="/img/razones.svg"/>
				</div>
				<div className={styles.separador} style={{backgroundColor:"#FFF"}}></div>
				<div className={styles.texto} style={{color:"#fff"}}>
				<div className={styles.textoTrivia}>En cada nivel encontrarás 2 razones reales para estudiar en UPN. Y en el último nivel, el más difícil, habrá solo 1 razón. En total son 7 Razones. Identifícalas correctamente y acumula mayor puntaje.</div>
				<Button className={styles.butonContainer} style={{marginTop:20}} texto="COMENCEMOS" invert={false} onClick={()=>{
					this.setState({layer:2});
				}}></Button>
				</div>
			</div>
		</div>);
		}
		if(this.state.layer == 2){
			layer = (<div className={styles.contenedor}>
				<button className={styles.cerrar} onClick={this.props.cerrar}>
					<img src="/img/close.svg" alt=""/>
				</button>
				<div className={styles.contenido}>
					<div className={styles.contenido_nivel}>
						<div className={styles.containerNivel}>
							<div className={styles.iconCandadoOpen}></div>
							<img src="/img/razones.svg"/>
							<Button style={{marginTop:12,width:120}} texto="NIVEL 1" onClick={this.onPressNivel1}></Button>
						</div>
						<div className={styles.containerNivel}>
							<div className={(this.state.current_nivel>1)?styles.iconCandadoOpen:styles.iconCandadoClose}></div>
							<img src="/img/razones.svg"/>
							<Button style={{marginTop:12,width:120}} texto="NIVEL 2" onClick={()=>{
								if(this.state.current_nivel>1)this.onPressNivel2();
							}} enabled={(this.state.current_nivel>1)?true:false}></Button>
						</div>
						<div className={styles.containerNivel}>
							<div className={(this.state.current_nivel>2)?styles.iconCandadoOpen:styles.iconCandadoClose}></div>
							<img src="/img/razones.svg"/>
							<Button style={{marginTop:12,width:120}} texto="NIVEL 3" onClick={()=>{
								if(this.state.current_nivel>2)this.onPressNivel3();
							}} enabled={(this.state.current_nivel>2)?true:false}></Button>
						</div>
						<div className={styles.containerNivel}>
							<div className={(this.state.current_nivel>3)?styles.iconCandadoOpen:styles.iconCandadoClose}></div>
							<img src="/img/razones.svg"/>
							<Button style={{marginTop:12,width:120}} texto="NIVEL 4" onClick={()=>{
								if(this.state.current_nivel>3)this.onPressNivel4();
							}} enabled={(this.state.current_nivel>3)?true:false}></Button>
						</div>
					</div>
			</div>
		</div>);
		}
		var limite = 2;
		var dto = this.state.data[this.state.nivel-1];
		if(this.state.layer == 3){
			layer = (<div className={styles.contenedor+" "+styles.contenedor_azul}>
				<button className={styles.cerrar} onClick={this.props.cerrar}>
					<img src="/img/close.svg" alt=""/>
				</button>
				<div className={styles.contenido_azul}>
				<h1>NIVEL {this.state.nivel}</h1>
				{this.state.nivel!=4 && <h2>Selecciona las 2 opciones correctas</h2>}
				{this.state.nivel==4 && <h2>Selecciona la opción correcta</h2>}
				<div className={styles.preguntas_container}>
					<div className={styles.pregunta_fila}>
						<Item opcion={dto.opcion1} disabled={(this.state.opciones>limite)?true:false} onOk={()=>{
							this.setState({correctas:this.state.correctas+1});
						}} onSelected={()=>{
							console.log(this.state.opciones);
							this.setState({opciones:this.state.opciones+1},this.checkCorrectas);
						}}></Item>
						<Item opcion={dto.opcion2} disabled={(this.state.opciones>limite)?true:false} onOk={()=>{
							this.setState({correctas:this.state.correctas+1});
						}} onSelected={()=>{
							this.setState({opciones:this.state.opciones+1},this.checkCorrectas);
						}}></Item>
					</div>
					<div className={styles.pregunta_fila}>
						<Item opcion={dto.opcion3} disabled={(this.state.opciones>limite)?true:false} onOk={()=>{
							this.setState({correctas:this.state.correctas+1});
						}} onSelected={()=>{
							console.log(this.state.opciones);
							this.setState({opciones:this.state.opciones+1},this.checkCorrectas);
						}}></Item>
					</div>
					<div className={styles.pregunta_fila}>
						<Item opcion={dto.opcion4} disabled={(this.state.opciones>limite)?true:false} onOk={()=>{
							this.setState({correctas:this.state.correctas+1});
						}} onSelected={()=>{
							console.log(this.state.opciones);
							this.setState({opciones:this.state.opciones+1},this.checkCorrectas);
						}}></Item>
						<Item opcion={dto.opcion5} disabled={(this.state.opciones>limite)?true:false} onOk={()=>{
							this.setState({correctas:this.state.correctas+1});
						}} onSelected={()=>{
							console.log(this.state.opciones);
							this.setState({opciones:this.state.opciones+1},this.checkCorrectas);
						}}></Item>
					</div>
					</div>
			</div>
		</div>)
		}

		return layer;
  }
}


export default component
