import { useEffect  } from 'react';
import styles from './estilo.module.sass'
import classnames from 'classnames';

import { useSelector, shallowEqual, useDispatch } from 'react-redux'
import { anunciarLogro2 } from 'store/sesion/accion'



const component = ({className, cerrar}) => {

	const dispatch = useDispatch()
	useEffect(() => {
		dispatch(anunciarLogro2(false))
	},[]);

	return <>

		<div className={styles.cuadro}>
			<div className={styles.contenedor}>
				<button className={styles.cerrar} onClick={cerrar}>
					<img src="/img/close.svg" alt=""/>
				</button>
				<div className={styles.contenido}>
						<h2>!LO LOGRASTE¡</h2>
						<div className={styles.texto}>
							Acumulaste
							todos los puntos,
							ya estas en
							el sorteo
						</div>
				</div>
			</div>
		</div>
	</>

}

export default component
