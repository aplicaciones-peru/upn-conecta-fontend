import { useState, useEffect } from 'react'
import Link from 'next/link'

import css from './estilo.module.sass'
import classnames from 'classnames'

import { registrarEvento } from 'lib/ga'


const menuItems = [
	{url:"juego", icono: "ico-juego.svg", titulo:"Juego Razones"},
	// {url:"contacto", icono: "ico-wsp.svg", titulo:"Contáctanos"},
	{url:"video", icono: "ico-video.svg", titulo:"Video UPN"},
	{url:"cita", icono: "ico-sobre.svg", titulo:"Cita vocacional"},
]


const component = ({children, className, subitemClick, ...props}) => {

	// const cx = classnames.bind(css)
	const [abierto, setAbierto] = useState(false)

	useEffect(() => {
		registrarEvento("submenu", abierto?"abierto":"cerrado")
	},[abierto]);

	return (
		<div className={classnames(css.contenedor, className, {[css.expandido]:abierto})}>
			<button
				type="button"
				className={classnames(css.botonMenu)}
				onClick={()=>setAbierto(!abierto)}>
				<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
					<rect y="14" width="32" height="3" rx="1.5" fill="black"/>
					<rect x="14" y="32" width="32" height="3" rx="1.5" transform="rotate(-90 14 32)" fill="black"/>
				</svg>

			</button>
			<div className={css.lista}>
				{
					menuItems.map((item, index) => (
						<button type="button" key={item.url} onClick={()=>setAbierto(false) || subitemClick(item.url)}>
							<i><img src={`/img/${item.icono}`} alt=""/></i>
							<span>{item.titulo+""}</span>
						</button>
					))
				}

			</div>
		</div>
	)
}


export default component
