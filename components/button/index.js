import React, { Component } from 'react';
import styles from './button.module.css';
import classnames from 'classnames'

export default class Button extends Component {
  constructor(props) {
        super(props);
  }
  render() {
    var enabled = true;
    if(this.props.enabled!=undefined){
      enabled=this.props.enabled;
    }

    return (
            <div style={{opacity:(enabled)?1:0.6,display:"flex",justifyContent:"center"}} className={this.props.className}>
            <div onClick={this.props.onClick} style={this.props.style} className={classnames((!this.props.invert)?styles.container:styles.container_yellow, styles.button)}>
            {this.props.texto}
            </div>
            </div>);
  }
}
