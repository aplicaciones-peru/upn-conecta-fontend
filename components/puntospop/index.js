import React, { Component } from 'react';
import styles from './popup.module.css';
import Button from '../button';

export default class PuntosPop extends Component {
  constructor(props) {
        super(props);
  }
  //<Button style={{marginTop:20}} texto="COMENCEMOS"></Button>
  render() {
    return (<div className={styles.container}>
              <div className={styles.closeButton}></div>
              <div className={styles.texto}>
                <div className={styles.topText}>
                <b>¡Explora y gana!</b><br/>
                Alcanza los <b>1000 puntos</b> y podrás ganar<br/>
                <b style={{color:"#FDBA30"}}>Membresías para <span style={{color:"red"}}>Nexflix</span>, Suscripciones para <span style={{color:"#0F901C"}}>Spotify</span> y vales de consumo para <span style={{color:"red"}}>Plaza Vea</span></b>
                </div>
                <ul>
                <li>
                  <div className={styles.label}>
                      <div className={styles.circulo}></div>
                      <div className={styles.desc}>Razones para estudiar en UPN</div>
                  </div>
                  <div className={styles.puntaje}>100 Puntos</div>
                </li>
                <li><div className={styles.label}><div className={styles.circulo}></div> Contacto con un asesor</div>  <div className={styles.puntaje}>250 Puntos</div></li>
                <li><div className={styles.label}><div className={styles.circulo}></div> Cita vocacional</div> <div className={styles.puntaje}>250 Puntos</div></li>
                <li><div className={styles.label}><div className={styles.circulo}></div> Videojuego UPN Conecta</div> <div className={styles.puntaje}>200 Puntos</div></li>
                <li><div className={styles.label}><div className={styles.circulo}></div> Charlas</div> <div className={styles.puntaje}>100 Puntos</div></li>
                <li><div className={styles.label}><div className={styles.circulo}></div> Testimoniales</div> <div className={styles.puntaje}>100 Puntos</div></li>
                </ul>
              </div>
            </div>);
  }
}
