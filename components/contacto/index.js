import { useState, useEffect } from 'react'
import css from './estilo.module.sass'
import classnames from 'classnames';

const component = ({className, cerrar}) => {
	return <>
		<div className={css.cuadro}>
			<div className={css.contenedor}>
				<button className={css.cerrar} onClick={cerrar}>
					<img src="/img/close.svg" alt=""/>
				</button>
				<div className={css.contenido}>
					Contacto
				</div>
			</div>
		</div>
	</>

}

export default component
