import React, { useState } from 'react';
import Link from 'next/link'

import css from './estilo.module.sass'
import classnames from 'classnames';
import { facultades } from 'lib/api'

import { useSelector, shallowEqual, useDispatch } from 'react-redux'
import { actualizarPuntaje } from 'store/puntaje/accion'

import Container from 'react-bootstrap/Container'

import { withRouter, useRouter } from 'next/router'
import { terminarSesion } from 'store/sesion/accion'

import { registrarEvento } from 'lib/ga'

// const facultades = [
// 	{url: "/facultades/negocios", titulo: "NEGOCIOS"},
// 	{url: "/facultades/ingenieria", titulo: "INGENIERÍA"},
// 	{url: "/facultades/comunicaciones", titulo: "COMUNICACIONES"},
// 	{url: "/facultades/salud", titulo: "SALUD"},
// 	{url: "/facultades/derecho", titulo: "DERECHO"},
// ]

const component = ({className, menuCerrar}) => {

	// const [nombre, setNombre] = useState("Carlos")
	// const [puntos, setPuntos] = useState(90)
	// const {nombre, }
	const router = useRouter()
	const {puntos, nombre, facultadActual} = useSelector(
		(state) =>({
			puntos: state.puntaje.total,
			nombre: state.usuario.nombres,
			facultadActual: state.sesion.facultad,
		}),
		shallowEqual
	)
	const cerrarSession = (e) => {
		dispatch(terminarSesion())
		router.push("/")
	}

/*<Link href="/inicio">
	<a onClick={menuCerrar}>
		INICIO
	</a>
</Link>*/




	// console.debug("puntos", puntos)

	const dispatch = useDispatch()

	console.debug("puntajes", puntos)
	const cerrarClick = evt => {
		// dispatch(actualizarPuntaje("prueba",50))
	}
	/*
	<Link href="/inicio">
	<button
		className={css.cerrar_sesion}
		onClick={ cerrarClick }>
		inicio
	</button>
	</Link>*/
	const principal = facultades.map( facultad => (
		<Link key={facultad.nombre} as={`/facultades/${facultad.nombre}`} href="/facultades/[facultad]">
			<a onClick={() => {
					menuCerrar()
					registrarEvento("menu", facultad.nombre)}
				}
				className={classnames({[css.activo]: facultadActual==facultad.nombre })}>
				{facultad.titulo}
			</a>
		</Link>
	))

	return <div className={classnames(className, css.contenedor)}>
		<div className={css.usuario}>
			<span>Bienvenido</span>
			<strong>{nombre}</strong>
		</div>
		<div className={css.puntaje}>
			<span>Tienes</span>
			<strong>{puntos} Puntos</strong>

			<Link href="/inicio" >
			<button className={css.cerrar_sesion} onClick={menuCerrar}>
				inicio
			</button>
			</Link>

			<button
				className={css.cerrar_sesion}
				onClick={ cerrarSession }>
				cerrar sesión
			</button>

		</div>
		<div className={css.titular}>
			<span>Facultades</span>
		</div>
		<div className={css.principal}>

			{principal}
		</div>
		<div className={css.secundario}>
			<div className={css.redes}>
				<a href="https://www.facebook.com/UPN"
					target="_blank"
					onClick={() => {
						registrarEvento("enlace", "facebook")
						menuCerrar()
					}}>
					<img src="/facebook.svg"/>
				</a>
				<a href="https://www.instagram.com/upn/?hl=es-la"
					target="_blank"
					onClick={() => {
						registrarEvento("enlace", "instagram")
						menuCerrar()
					}}>
					<img src="/instagram.svg"/>
				</a>
			</div>
			<div className="otros">
				<a href="https://www.upn.edu.pe/politicas-y-privacidad"
					target="_blank"
					onClick={() => {
						registrarEvento("enlace", "politicas y privacidad")
						menuCerrar()
					}}>políticas y privacidad</a>
			</div>
		</div>
		<button className={css.cerrar} onClick={menuCerrar}>
			<svg width="50" height="50" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
			<circle cx="30" cy="30" r="30" fill="#FDBA30"/>
			<rect x="18" y="39.8996" width="32" height="3" rx="1.5" transform="rotate(-45 18 39.8996)" fill="#000"/>
			<rect x="40.6274" y="42.728" width="32" height="3" rx="1.5" transform="rotate(-135 40.6274 42.728)" fill="#000"/>
			</svg>
		</button>
	</div>
}


export default component
