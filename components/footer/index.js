import { useState } from 'react'
import Link from 'next/link'

import css from './estilo.module.sass'
import classnames from 'classnames'

import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Image from 'react-bootstrap/Image'
import Button from 'react-bootstrap/Button'


// export default function component({className}) {

// 	const [abierto, setAbierto] = useState(false)

// 	return <Container fluid className={classnames(className)}>
// 		<Row className={classnames("justify-content-between")}>
// 			<Col xs="auto" className={classnames(abierto:css.abierto)}>
// 				<div className={css.menu}>
// 					<Button className={classnames(css.botonMenu)}>
// 						<Image src="img/ico-cruz.svg" />
// 					</Button>
// 					<div className="lista">
// 						<a href="#">
// 							<i><img src="img/ico-juego.svg" alt=""/></i>
// 							<span>Juego Razones</span>
// 						</a>
// 						<a href="#">
// 							<i><img src="img/ico-wsp.svg" alt=""/></i>
// 							<span>Contáctanos</span>
// 						</a>
// 						<a href="#">
// 							<i><img src="img/ico-video.svg" alt=""/></i>
// 							<span>Video Tour</span>
// 						</a>
// 						<a href="#">
// 							<i><img src="img/ico-sobre.svg" alt=""/></i>
// 							<span>Cita vocacional</span>
// 						</a>
// 					</div>
// 				</div>
// 			</Col>
// 		</Row>
// 	</Container>
// }


const component = ({children, className, subitemClick, ...props}) => {

	// const cx = classnames.bind(css)
	const [abierto, setAbierto] = useState(false)

	return (
	<div className={classnames(css.contenedor, className)}>
		<div className={css.contenido}>
		<If condition={ props.contenido}>
			{props.contenido}
		</If>
		</div>
	</div>
)}


export default component
