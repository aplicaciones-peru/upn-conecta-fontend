import { useState, useEffect } from 'react';
import css from './estilo.module.sass'
import classnames from 'classnames'
import { withRouter, useRouter } from 'next/router'

import { useDispatch } from 'react-redux'
import { actualizarDatos } from 'store/usuario/accion'
import { iniciarSesion, actualizarNivel } from 'store/sesion/accion'

import Loginbox from '../loginbox'
import Validacion from '../validacion'
import Registro from '../registro'

import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Image from 'react-bootstrap/Image'
import Button from 'react-bootstrap/Button'

import { event } from "next-ga/dist/analytics/prod";

import { registrarEvento } from 'lib/ga'


// class Index extends React.Component {
// 	constructor() {
// 		super();
// 		this.state = {
// 			fase: 0
// 		}

// 	}

// 	mensaje(){
// 		return <Row className={classnames(css.mensaje, "justify-content-center")}>
// 			<Col xs="auto" className={classnames("text-center")}>
// 				<div className={classnames(css.texto, "p-4", "mb-3", "rounded-pill", "text-center")}>
// 					Visita nuestras facultades.
// 					Acumula puntos y
// 					gana muchos premios
// 				</div>
// 				<Button
// 					onClick={this.participaClick}
// 					variant="dark"
// 					className={classnames(css.boton, "d-block", "rounded-pill", "mx-auto", "py-3", "px-5")}>
// 					¡PARTICIPA YA!
// 				</Button>
// 			</Col>
// 		</Row>
// 	}

// 	boton1Click = (e) => {
// 		e.preventDefault();
// 		console.log(this.state.fase);
// 		this.setState({
// 			fase: 2
// 		})
// 	}

// 	ingresa = (e) => {
// 		e.preventDefault();
// 		console.log(this.state.fase);
// 		this.setState({
// 			fase: 3
// 		})
// 	}
// 	conectate = (e) => {
// 		e.preventDefault();
// 		const {router} = this.props
// 		router.push('/inicio')
// 	}

// 	render() {

// 		const {fase} = this.state

// 		return <Choose>
// 			<When condition={(fase == 1)}>
// 				{this.mensaje()}
// 			</When>
// 			<When condition={(fase == 2)}>

// 				<Validacion onContinuar={this.ingresa}/>
// 			</When>
// 			<When condition={(fase == 3)}>

// 				<Registro onContinuar={this.conectate}/>
// 			</When>
// 		</Choose>
// 	}

// 	componentDidMount() {
// 		this.setState({
// 			fase: 1
// 		})
// 	}
// }

// export default withRouter(Index);
const component = ({ leyenda }) => {

	const [fase, cambiarFase] = useState(1)
	const [datos, cambiarDatos] = useState({})
	const [movil, cambiarMovil] = useState(false)


	const router = useRouter()

	useEffect(() => {
		const matchMediaChange = e => { cambiarMovil(e.matches) }
		cambiarMovil(window.matchMedia("(max-width: 768px)").matches)
		// window.matchMedia("(max-width: 768px)").addEventListener( "change",  matchMediaChange)
		window.matchMedia("(max-width: 768px)").addListener( matchMediaChange)
		return () => {window.matchMedia("(max-width: 768px)").removeListener(matchMediaChange)}
		// return () => {window.matchMedia("(max-width: 768px)").removeEventListener( "change",  matchMediaChange)}
	},[]);

	useEffect(() => { leyenda(!movil || fase == 1) }, [movil, fase])
	useEffect(() => {

		const nombresFases = [
			"",
			"validacion",
			"validacion",
			"registro",
			"mensaje"
		]

		registrarEvento("navegacion", "login " + nombresFases[fase])

	}, [fase])

	const dispatch = useDispatch()

	const participaClick = (e) => {
		e.preventDefault()
		cambiarFase(2)
	}
	const ingresa = info => {
		if (info.estado == "OK") {
			cambiarDatos(info.datos)
			dispatch(actualizarDatos(info.datos))
			dispatch(actualizarNivel(info.datos.nivel))
			dispatch(iniciarSesion())
			// if(info.datos.actualizado){
			// 	router.push("/instrucciones")

			// cambiarFase(4)
			// }else{
			// 	cambiarFase(3)
			// }
			router.push("/instrucciones")
		}else{
			cambiarFase(3)
		}
	}
	const conectate = info => {
			// e.preventDefault();
		// const {router} = this.props
		// router.push('/inicio')
		// cambiarFase(4) // modo registro
		dispatch(actualizarDatos(info.datos))
		dispatch(actualizarNivel(info.datos.nivel))
		router.push("/instrucciones") // modo evento

	}

	const mensajeInicial = () => (
		<Row className={classnames(css.mensaje, "justify-content-center")}>
			<Col xs="auto" className={classnames("text-center")}>
				<Button
					onClick={participaClick}
					variant="dark"
					className={classnames(css.boton, "d-block", "rounded-pill", "mx-auto", "py-3", "px-5")}>
					¡PARTICIPA YA!
				</Button>
			</Col>
		</Row>
	)

	const mensajeFinal = () => (
		<div className={css.contenedor}>
			<div className={css.titular}>
				Ingreso correcto
			</div>
			<hr/>
			<div className={css.texto}>
				Gracias por visitarnos.<br/>
				<div className={css.fecha}>¡TE ESPERAMOS EL <br/>5 de DICIEMBRE!</div>
			</div>
		</div>
	)

	// {/* <button>¡CONECTATE!</button> */}

	return <>
		<div className={css.contenido}>
			<Choose>
				<When condition={(fase == 1)}>
					<Choose>
						<When condition={movil}>
							{mensajeInicial()}
						</When>
						<Otherwise>
							<Validacion onContinuar={ingresa}/>
						</Otherwise>
					</Choose>
				</When>
				<When condition={(fase == 2)}>
					<Validacion onContinuar={ingresa}/>
				</When>
				<When condition={(fase == 3)}>
					<Registro onContinuar={conectate} datos={datos}/>
				</When>
				<When condition={(fase == 4)}>
					{mensajeFinal()}
				</When>
			</Choose>
		</div>
	</>
}

export default withRouter(component);
