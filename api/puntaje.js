import { API } from './valores'

import { store } from 'store/'

export const actualizar = (evento, puntos) => {
	// const prospecto = 23382
	const estado = store.getState();
	const prospecto = estado.usuario.id
	// console.debug(estado)

	const url = `${API}/puntaje/${prospecto}`
	const request = {
		method:'POST',
		body: JSON.stringify({
			evento,
			puntos
		}),
		headers: {"Content-type":"application/json; charset=UTF-8"}
	};
	return fetch(url, request)
		.then(response => response.json())
}

export const consultarJuego = () => {
	console.debug("consultarJuego")
	// const prospecto = 23382
	const estado = store.getState();
	const prospecto = estado.usuario.id
	// console.debug(estado)

	const url = `${API}/juego/${prospecto}`
	return fetch(url)
		.then(response => response.json())
}

export const prueba = () => {
	// const prospecto = 23382

	const estado = store.getState();

	console.log(estado)
}
