# UPN Conecta
_Landing desarrollada para registro de prospectos para la Universidad Privada del Norte_
## Comenzando

### Pre-requisitos
* [Nodejs](https://nodejs.org/es/)
* [NPM](https://www.npmjs.com/)
* Servidor Web


### Instalación

_Clonar el proyecto en la carpeta de destino_
```
$ git clone <url git> conecta
```

_Instalar las dependencias_
```
$ npm install
```

_Configurar lo valores de entorno_
```
# .env.local
NEXT_PUBLIC_URL_API="//api.upnconecta.pe"
NEXT_PUBLIC_GA_ID = "G-CE5RRYHKF7"
NEXT_PUBLIC_URL_JUEGO = ""

```
> Donde : \
> `NEXT_PUBLIC_URL_API` es la ubicación de los servicios backend \
> `NEXT_PUBLIC_GA_ID` es el id de Google Analytics
> `NEXT_PUBLIC_URL_JUEGO` es el url del juego


_Compilar el proyecto_
```
$ npm run build
```

_Arrancart la aplicación_
```
$ npm run start
```

_Al acceder a la url destinada se debe mostrar la pantalla inicial de la landing_
## Despliegue
_En caso de deplegar en un entorno Serverless_

_Renderizar la aplicacion de manera estática_
```
$ next export
```
_Copiar la carpeta `out` a la ubicación necesaria_

El folder `juego` se tiene que servir de manera independiente en un servidor apache (o en el mismo server) y la ruta debe de ser colocada en `.env.local` mediante la variable `NEXT_PUBLIC_URL_JUEGO` ya que es cargado mediante un iframe


## Construido con
* [Nodejs](https://nodejs.org/es/)
* [NPM](https://www.npmjs.com/)
* [Reactjs](https://es.reactjs.org/)
* [NextJs](https://nextjs.org/)
* [Bootstrap](https://getbootstrap.com/)

---
